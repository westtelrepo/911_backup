#!/bin/bash
VERSION='1.3.2-full'

#==================================================================
# This script is used to backup files
# in the 911 system to the PSAP backup server.
#
# Author Matt Nichols - Aug 2016 v1.0
#
# Notes: setup places a cron file in /etc/cron.d/911_backup
# Requires ssh to have config in ./ssh_config/config
#
# 1.3.2 backup full freeswitch/freeswitch_config/ directory - 12/2017
#==================================================================

readonly SET_ENV_PROMPT='Please set both $PSAP_SITE_ID and $PSAP_SERVER_ID [in the file /etc/environment]'

fn_setup_env() {
	echo ''
	while true; do
		read -r -p 'Please enter PSAP site ID (e.g. CO-990): ' PSAP_SITE_ID
		if [[ "$PSAP_SITE_ID" =~ [a-zA-Z0-9-]+ ]]; then
			break
		else
			echo 'Invalid format for site ID'
		fi
	done
	echo
	while true; do
		read -r -p 'Please enter PSAP server ID (e.g. CO-990-1): ' PSAP_SERVER_ID
		if [[ "$PSAP_SERVER_ID" =~ [a-zA-Z0-9-]+ ]]; then
			break
		else
			echo 'Invalid format for server ID'
		fi
	done
}

fn_check_env() {
	if [ -z "$PSAP_SITE_ID" -o -z "$PSAP_SERVER_ID" ]; then
		echo
		echo "$SET_ENV_PROMPT" > /dev/stderr
		echo
		exit 1
	fi
}

fn_save_env() {
	local file='/etc/environment.tmp.6gQP7XpG26uyubEE'
	< /etc/environment sed -e '/^PSAP_SITE_ID=/d' -e '/^PSAP_SERVER_ID=/d' > "$file"
	echo 'PSAP_SERVER_ID="'"$PSAP_SERVER_ID"'"' >> "$file"
	echo 'PSAP_SITE_ID="'"$PSAP_SITE_ID"'"' >> "$file"
	mv "$file" /etc/environment
}


readonly SSH_USER="backup_${PSAP_SERVER_ID}"

readonly SSH_IDENTITY="$(dirname "$0")/ssh_config/911_backup.sh.key"
readonly SSH="ssh -F $(dirname "$0")/ssh_config/config psap_server_$PSAP_SERVER_ID@jump ssh "
readonly REMOTE_SERVER="backup"
readonly ADD_USER_SERVER="psap_add_user"
readonly REMOTE_FOLDER="/backups/current/$PSAP_SITE_ID/$PSAP_SERVER_ID"
readonly REMOTE_LOCATION="$REMOTE_SERVER:$REMOTE_FOLDER"

function fn_confirm () {
	local yn
	while true; do
		echo
		read -r -p "$1 [Y/n]? " yn

		case "$yn" in
			[Yy] ) return 0;; # 0 is true here
			[Yy][Ee][Ss] ) return 0;;
			'' ) return 0;;
			[Nn] ) return 1;;
			[Nn][Oo] ) return 1;;
			* ) break;;
		esac
	done
}

fn_backup () {
	fn_check_env
	local INCLUDE1=() EXCLUDE1=()

	INCLUDE1+=('/')
	EXCLUDE1+=('/home/experient/tshark')
	EXCLUDE1+=('/datadisk1/tshark')
	EXCLUDE1+=('/dev/' '/proc/' '/sys/' '/tmp/' '/run/' '/mnt/' '/media/' '/lost+found/' '/var/')
	#EXCLUDE1+=('*.pcap')

	# INCLUDE1+=('/etc/')
	# # if resolv.conf is a symbolic link, then dns nameservers are maintained by resolvconf
	# # and the settings are in /etc/network/interfaces (assuming static ip on server)
	# if [ -L '/etc/resolv.conf' ]; then
	# 	EXCLUDE1+=('/etc/resolv.conf')
	# fi
	# EXCLUDE1+=('/etc/prelink.cache')

	# INCLUDE1+=('/datadisk1/ng911/')
	# EXCLUDE1+=('/datadisk1/ng911/emails/')
	# EXCLUDE1+=('/datadisk1/ng911/src/')
	# EXCLUDE1+=('/datadisk1/ng911/logs/cli/')
	# EXCLUDE1+=('/datadisk1/ng911/logs/*.xml')
	# EXCLUDE1+=('/datadisk1/ng911/logs/*.dbg')
	# EXCLUDE1+=('/datadisk1/ng911/logs/*.xm$')
	# EXCLUDE1+=('/datadisk1/ng911/ng911.pid')
	# EXCLUDE1+=('core.*')
	# EXCLUDE1+=('core')
	# EXCLUDE1+=('core-*')

	# INCLUDE1+=('/datadisk1/tftpboot/')
	# EXCLUDE1+=('/datadisk1/tftpboot/polycom/**.zip')
	# #EXCLUDE1+=('/datadisk1/tftpboot/polycom/*/*.log')
	# #EXCLUDE1+=('/datadisk1/tftpboot/polycom/*/0004f2*-calls.xml')

	INCLUDE1+=('/datadisk1/freeswitch/freeswitch_config/conf/')
	INCLUDE1+=('/datadisk1/freeswitch/freeswitch_config/scripts/')
	INCLUDE1+=('/datadisk1/freeswitch/freeswitch_config/*.xml')


	rsync -e "$SSH" --one-file-system -avzRHAX --no-acls --no-xattrs --delete --delete-excluded --stats "${EXCLUDE1[@]/#/--exclude=}" \
		"${INCLUDE1[@]}" "$REMOTE_LOCATION"
}

fn_disp_box () {
	echo "-----------------------------------------------------------------------"
	echo " 911_backup.sh - Version $VERSION"
	echo
	echo " Backs up files from the 9-1-1 server to"
	echo " $REMOTE_LOCATION"
	echo "-----------------------------------------------------------------------"
}

fn_setup() {
	if [ -z "$PSAP_SITE_ID" -o -z "$PSAP_SERVER_ID" ]; then
		fn_setup_env
		fn_save_env
	fi
	echo
	echo "Public IP: $(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com)"
	echo
	while true; do
		echo "PSAP_SITE_ID: $PSAP_SITE_ID"
		echo "PSAP_SERVER_ID: $PSAP_SERVER_ID"
		if ! fn_confirm 'Are the above site and server IDs correct'; then
			fn_setup_env
		else
			fn_save_env
			break
		fi
	done

	fn_check_env

	if ! fn_confirm "Create a new user and ssh key on the PSAP backup server"; then
		return 1
	fi

	local ident_temp="$SSH_IDENTITY.tmp"
	rm -f "$ident_temp" "$ident_temp.pub"
	mkdir -p "$(dirname "$ident_temp")"

	ssh-keygen -t ed25519 -N '' -f "$ident_temp" -C "backup user for $PSAP_SERVER_ID at $PSAP_SITE_ID" > /dev/null 2> /dev/null || \
		ssh-keygen -t rsa -b 4096 -N '' -f "$ident_temp" -C "backup user for $PSAP_SERVER_ID at $PSAP_SITE_ID" > /dev/null || \
		{ printf '%s\n\n' 'Could not generate ssh identity (key)' > /dev/stderr; exit 1; }
	echo
	echo "Login to create PSAP backup account"
	printf '%s\n%s\n%s\n' "$PSAP_SITE_ID" "$PSAP_SERVER_ID" "$(cat $ident_temp.pub)" | \
		ssh -F "$(dirname "$0")/ssh_config/config" psap_add_user@jump add-user || \
		{ printf '%s\n\n' 'Could not create new user, exiting...' > /dev/stderr; exit 1; }

	[ -f "$SSH_IDENTITY" ] && shred "$SSH_IDENTITY"
	rm -f "$SSH_IDENTITY" "$SSH_IDENTITY.pub"
	mkdir -p "$(dirname "$SSH_IDENTITY")"
	mv "$ident_temp" "$SSH_IDENTITY"
	mv "$ident_temp.pub" "$SSH_IDENTITY.pub"

	echo "$(( ( RANDOM % 60 ) )) * * * * root $(cd "$(dirname "$0")"; pwd)/$(basename "$0") -b" > /etc/cron.d/911_backup
	echo 'Added to cron (filename /etc/cron.d/911_backup)'
}

fn_menu () {
	fn_disp_box
	local selection
	select selection in "Backup files" "Setup" "Exit"; do
		case "$selection" in
			"Backup files" )
				fn_backup;;
			"Exit" )
				exit 0;;
			"Setup" )
				fn_setup;;
		esac

		echo
		break
	done
}

fn_arg_parsing () {
	if [ "$#" -ne 1 ]; then
		echo 'Too many arguments!' > /dev/stderr
		exit 1
	fi

	case "$1" in
		'-h')
			fn_disp_box
			echo
			echo 'Usage: rsync_psap_to_backup.sh [option]'
			echo "Version: $VERSION"
			echo
			echo '-v = Display version'
			echo '-b = Backup files'
			echo '-h = Display help'
			echo '[DEPRECATED] --backup-with-delay = Backs up, but has a random delay first. Used in cron'
			echo
			echo 'If no command-line option is used a menu will display'
			echo
			exit;;
		'-b')
			fn_backup;;
		'--backup-with-delay')
			# delay is done down below, before lock is activated
			fn_backup;;
		*)
			echo "Unknown argument '$1'" > /dev/stderr
			exit 1;;
	esac
}

fn_main () {
	if [ "$#" -gt 0 ]; then
		fn_arg_parsing "$@"
	elif [ "$#" -eq 0 ]; then
		fn_menu
	fi
}

# allow -v to work even if lock is held or script is not ran as root
if [ "$1" = "-v" -a "$#" -eq 1 ]; then
	echo
	echo "Version $VERSION"
	echo
	exit
fi

if [ "$#" -eq 1 -a "$1" = '--backup-with-delay' ]; then
	sleep $(( ( RANDOM % 60 ) ))m
fi

if [ "$(whoami)" != "root" ]; then
	echo
	echo "This script must be run as root (e.g., using 'sudo')"
	echo
	exit 1
fi

(
	flock -n 200 || { printf '\n%s\n\n' 'This script is currently in use' > /dev/stderr; exit 1; }
	fn_main "$@"
) 200>/var/lock/911_backup.O5Qc8gwYEN5gwOVj.lock
